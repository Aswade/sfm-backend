const express = require('express');
const router = express.Router();
const driver = require('../dbDriver/dbDriver');
const dbDriver = new driver();
const mediaPlayerClass = require('./mediaPlayerClass/mediaPlayer');
const mediaPlayer = new mediaPlayerClass();

router.post('/', async (req, res, next) => {
  let pagination = parseInt(req.body.pagination) || 0;
  let regQuery = new RegExp(`\\w`);
  let result = await dbDriver.selectFromDb('file', regQuery, {
    pagination: pagination,
  });
  let status = result.status;
  let response = result.data || result.message;
  res.status(status).json(JSON.stringify({ response: response }));
});

router.post('/play', async (req, res, next) => {
  let musicSrc = req.body.musicSrc;
  let startFrom = req.body.startFrom || 0;
  mediaPlayer.streamMusic(res, musicSrc, { startFrom: startFrom });
});
router.post('/playEntire', async (req, res, next) => {
  let musicSrc = req.body.musicSrc;
  mediaPlayer.streamEntireMusic(res, musicSrc);
});

router.post('/search', async (req, res, next) => {
  let pagination = parseInt(req.body.pagination) || 0;
  let query = req.body.query;
  let searchFor = req.body.searchFor;
  let regQuery = new RegExp(`${query}`, 'i');
  let result = await dbDriver.selectFromDb(searchFor, regQuery, {
    pagination: pagination,
  });
  let status = result.status;
  let response = result.data || result.message;

  res.status(status).json(JSON.stringify({ response: response }));
});

router.post('/shufflePlaylist', async (req, res, next) => {
  mediaPlayer.shufflePlaylist().nextSong().streamMusic(res);
});

module.exports = router;
