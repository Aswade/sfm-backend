function reportException(exceptionObj){
  try {
    if(!Object.keys(exceptionObj).length){
      throw 'exception object is empty';
      return this;
    }
    
    let eType = exceptionObj.type;
    let eKeyword = exceptionObj.keyword;
    let eOrigin = exceptionObj.eOrigin;
    let eMsg = exceptionObj.msg;
    exceptionExist = (eType,eKeyword) =>{
      if(!eType.length){
        return false;
      }
      //console.log(eType);
      for (let i = 0; i < eType.length; i++) {
        if(eType[i].keyword === eKeyword){
          return true;
        }
      }
    }
    let eExist = exceptionExist(this.exceptions[eType],eKeyword);


    if (eExist) {
      console.log('such exception already exists');
      return this;
    }
    
    this.exceptions[eType].push({
      type:eType,
      origin:eOrigin,
      keyword:eKeyword,
      msg:eMsg
    });
  } catch (e) {
    this.exceptions.errors.push({"reportExceptionError":e});
  }
}


module.exports = reportException;