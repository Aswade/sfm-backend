"use strict";

function shufflePlaylist() {
  try {
    if (!this.playlist.length) {
      this.reportException({
        type:"notices",
        keyword:"PIE",
        eOrigin:'mediaPlayer.shufflePlaylist()',
        msg:'playlist is empty'
      });
      return this;
    }
    let originalPlaylist = this.playlist;
    
    let shufflePlaylist = originalPlaylist.slice();
    let notShuffledLength = shufflePlaylist.length;
    while(notShuffledLength){
      let randomIndex = Math.floor(Math.random() * notShuffledLength--);
      
      let toShuffle = shufflePlaylist[notShuffledLength];
      shufflePlaylist[notShuffledLength] = shufflePlaylist[randomIndex];
      shufflePlaylist[randomIndex] = toShuffle;
    }
    this.addToPlaylist(shufflePlaylist,true);
    return this;
  } catch (e) {
    this.reportException({
      type:"errors",
      keyword:"ME",
      eOrigin:'mediaPlayer.shufflePlaylist()',
      msg:e
    });
  }
}


module.exports = shufflePlaylist;