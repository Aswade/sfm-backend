async function streamMusic(res, song, options = {}) {
  try {
    let fs = this.fs;
    let musicSrc = './api/' + song;
    /*let musicMetadata = await this.musicMetadata.parseFile(musicSrc);
    let musicExt = musicMetadata.format.container.split('/')[1];*/

    if (!fs.existsSync(musicSrc)) throw 'file does not exist';
    /*let fileStats = fs.statSync(musicSrc, (error, stats) => {
      if (error) {
        throw error;
      }
      return stats;
    });*/
    resCode = 206;
    resHeader = {
      'Content-Type': 'audio/mp3',
    };
    let audio = fs.createReadStream(musicSrc, { encoding: 'base64' });
    res.status(resCode).header(resHeader);

    audio.pipe(res);
  } catch (e) {
    console.log(e);
    resHeader = { 'Content-Type': 'text/json' };
    status = 500;
    errors = e;

    //console.log(errors);
    res.status(status).json(JSON.stringify({ error: errors }));
  }
}

module.exports = streamMusic;
