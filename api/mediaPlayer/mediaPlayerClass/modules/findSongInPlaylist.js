function findSongInPlaylist(songId) {
  let playlist = this.playlist;
  for (let song of playlist) {
    if(song._id == songId)  return song;
  }
  return 'no such song';
}


module.exports = findSongInPlaylist;