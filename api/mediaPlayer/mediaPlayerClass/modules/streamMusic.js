async function streamMusic(res,song,options={}) {
  
  try {
    let chunkSize = (1024 * 1024);

    let startSrcFrom = (options.startFrom !== 0)? parseInt(options.startFrom)*chunkSize : 0;
    let endSrc = startSrcFrom + chunkSize;
    let fs = this.fs;
    console.log(`start:${startSrcFrom}, end:${endSrc}`);

    let musicSrc = './api/'+song;
    /*let musicMetadata = await this.musicMetadata.parseFile(musicSrc);
    let musicExt = musicMetadata.format.container.split('/')[1];*/
    console.log(musicMetadata);
    if(!fs.existsSync(musicSrc)) throw 'file does not exist';
    let fileStats = fs.statSync(musicSrc, (error, stats) => {
      if (error) {
        throw error;
      }
      return stats;
    });
    let howMuch = parseInt(fileStats.size)/chunkSize;
    console.log(howMuch);
    resCode = 206;
    resHeader = {
      'Content-Type': 'audio/mp3',
    };
    let audio = fs.createReadStream(musicSrc,{encoding:'base64',start:startSrcFrom,end:endSrc});
    res.status(resCode).header(resHeader);

    res.write(`{"timesToSend":"${howMuch}"}`);
    audio.pipe(res);
    
  } catch (e) {
    console.log(e);
    resHeader = {'Content-Type': 'text/json'};
    status = 500;
    errors = e.message;
    
    
    //console.log(errors);
    res.status(status).json(JSON.stringify({error:errors}));
  }

}


module.exports = streamMusic;
/*fs.readFile(musicSrc, 'base64',function (err,data) {
  if (err) {
    console.log(err);
    return 'couldnt send song';
  }
  res.send(data);
});*/