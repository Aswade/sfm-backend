class mediaPlayer {};

mediaPlayer.prototype.fs = require('fs');
mediaPlayer.prototype.musicMetadata = require('music-metadata');
mediaPlayer.prototype.reportException = require('./modules/reportException');
mediaPlayer.prototype.findSongInPlaylist = require('./modules/findSongInPlaylist');
mediaPlayer.prototype.streamMusic = require('./modules/streamMusic');
mediaPlayer.prototype.streamEntireMusic = require('./modules/streamEntireMusic');
mediaPlayer.prototype.shufflePlaylist = require('./modules/shufflePlaylist');


module.exports = mediaPlayer;