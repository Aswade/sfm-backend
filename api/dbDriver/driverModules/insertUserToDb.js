async function insertUserToDb(userInfo) {
  try {
    const user = new this.schemas.user({
      _id: new this.mongoose.Types.ObjectId(),
      nick: userInfo.nick,
      password: userInfo.password,
    });

    let isItSaved = await user
      .save()
      .then((result) => {
        console.log('user inserted to db;', result);
        return 'user inserted to db';
      })
      .catch((error) => {
        console.log('something went wrong, error:', error);
        return 'something went wrong: ' + error;
      });
    return isItSaved;
  } catch (e) {
    console.log('something went wrong, error:', e);
    return e;
  }
}

module.exports = insertUserToDb;
