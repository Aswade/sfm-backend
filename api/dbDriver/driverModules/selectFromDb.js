async function selectFromDb(
  searchFor = 'everyColl',
  regQuery = '',
  options = { pagination: 0 }
) {
  try {
    let pagination = options.pagination;
    let searchScope = {};
    const selectFields = {
      file: '_id author title genre filePath',
      user: '_id nick',
    };
    const queryObj = {
      file: { $or: [{ author: regQuery }, { title: regQuery }] },
      user: { nick: regQuery },
    };

    if (searchFor === 'everyColl') {
      searchScope = this.schemas;
    } else {
      searchScope = { [searchFor]: this.schemas[searchFor] };
      if (!searchScope[searchFor]) return { status: 500, message: 'no schema' };
    }

    let resObj = { status: 200, data: new Array() };
    for (let schemaKey in searchScope) {
      let docs = await returnData(
        searchScope[schemaKey],
        queryObj[schemaKey],
        selectFields[schemaKey],
        pagination,
        schemaKey
      );

      resObj.data = resObj.data.concat(docs);
    }
    return resObj;
  } catch (e) {
    console.log(e);
    return { status: 500, message: e.message };
  }
}
function returnData(schema, query, select, pagination, type) {
  return new Promise((resolve, reject) => {
    schema
      .find(query)
      .select(select)
      .skip(pagination)
      .limit(12)
      .exec()
      .then((docs) => {
        if (docs.length === 0) {
          resolve(['no result']);
        }
        let docsMapped = docs.map((doc) => {
          typedDoc = JSON.parse(JSON.stringify(doc));
          typedDoc.Type = type;
          return typedDoc;
        });
        resolve(docsMapped);
      })
      .catch((error) => {
        console.log(error);
        reject({ status: 500, error: error.message });
      });
  });
}
module.exports = selectFromDb;
