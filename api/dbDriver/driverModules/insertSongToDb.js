async function insertSongToDb(fileInfo) {
  try {
    const file = new this.schemas.file({
      _id: new this.mongoose.Types.ObjectId(),
      author: fileInfo.author,
      title: fileInfo.title,
      genre: fileInfo.genre || 'uncategorized',
      filePath: fileInfo.filePath,
    });

    let isItSaved = await file
      .save()
      .then((result) => {
        console.log('data inserted to db;', result);
        return 'data inserted to db';
      })
      .catch((error) => {
        console.log('something went wrong, error:', error);
        return 'something went wrong: ' + error;
      });
    return isItSaved;
  } catch (e) {
    console.log('something went wrong, error:', e);
    return e;
  }
}

module.exports = insertSongToDb;
