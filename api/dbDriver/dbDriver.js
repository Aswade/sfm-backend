class dbDriver {}

dbDriver.prototype.mongoose = require('mongoose');

dbDriver.prototype.schemas = {};
dbDriver.prototype.schemas.file = require('./schemas/file');
dbDriver.prototype.schemas.user = require('./schemas/user');
dbDriver.prototype.insertSongToDb = require('./driverModules/insertSongToDb');
dbDriver.prototype.insertUserToDb = require('./driverModules/insertUserToDb');

dbDriver.prototype.selectFromDb = require('./driverModules/selectFromDb');
dbDriver.prototype.resetFileSend = require('./driverModules/resetFileSend');

module.exports = dbDriver;
