const mongoose = require('mongoose');

const fileSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  author: String,
  title: String,
  genre: String,
  filePath: String
});

module.exports = mongoose.model('File',fileSchema);