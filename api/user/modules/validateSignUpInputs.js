async function validateSignUpInputs({ nick, password, rePassword }) {
  let nickRegEx = /^[A-Za-z0-9]{3,24}$/;
  let passwordRegEx =
    /(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}/;

  if (!nick.match(nickRegEx)) {
    return 'nick is bad';
  }

  let isNickTaken = await dbDriver.schemas.user.find({ nick: nick });

  if (isNickTaken.length > 0) {
    return 'nick is already taken';
  }
  if (!password.match(passwordRegEx)) {
    return 'password is bad';
  }
  if (password !== rePassword) {
    return 'passwords are not the same';
  }

  return 'VALID';
}

module.exports = validateSignUpInputs;
