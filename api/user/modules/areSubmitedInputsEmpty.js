function areSubmitedInputsEmpty(submitedInputs) {
  for (let key in submitedInputs) {
    if (key === 'author' || key === 'title' || key === 'genre') {
      if (
        typeof submitedInputs[key] !== 'string' ||
        submitedInputs[key] === ''
      ) {
        return 'Some inputs are empty';
      }
    } else if (key === 'file') {
      if (
        typeof submitedInputs[key] !== 'object' ||
        Object.keys(submitedInputs[key]).length === 0
      ) {
        return 'File is empty';
      }
    }
  }
  return 'NOT_EMPTY';
}

module.exports = areSubmitedInputsEmpty;
