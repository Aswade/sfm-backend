async function logInUser({ nick, password }) {
  let isSuchUser = await dbDriver.schemas.user.find({ nick: nick });

  if (isSuchUser.length < 1) {
    return 'login failed due to wrong nickname or password';
  }

  let isPasswordCorrect = await bcrypt.compare(password, user[0].password);

  if (!isPasswordCorrect) {
    return 'login failed due to wrong nickname or password';
  }

  return 'LOGED_IN';
}

module.exports = logInUser;
