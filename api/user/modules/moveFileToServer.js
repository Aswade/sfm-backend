function moveFileToServer(file) {
  let fileName = file.name;
  let splitedFileName = fileName.split('.');
  let ext = splitedFileName[splitedFileName.length - 1];

  let uniqueName = (+new Date()).toString(36);
  let newFilePath = `./api/sfmFiles/music/${uniqueName}.${ext}`;
  let result = file.mv(newFilePath, (error) => {
    if (error) {
      console.log(error);
      return `ERROR:${error}`;
    }
    return 'MOVED';
  });
  return { result: result, filePath: newFilePath };
}

module.exports = moveFileToServer;
