async function insertSongToDbHandler({ author, title, genre, filePath }) {
  let isInserted = await dbDriver.insertSongToDb({
    author: author,
    title: title,
    genre: genre,
    filePath: filePath,
  });

  if (isInserted !== 'data inserted to db') {
    let rmError = '';
    fs.rm(newFilePath, { recursive: true, maxRetries: 2 }, (err) => {
      if (err) {
        rmError = err;
        console.log(`file:${newFilePath} could not be deleted`);
      }
    });
    console.log(`dbError:${isInserted}, rmError:${rmError}`);
    return `dbError:${isInserted}, rmError:${rmError}`;
  }
  return 'INSERTED';
}

module.exports = insertSongToDbHandler;
