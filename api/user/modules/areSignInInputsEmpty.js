function areSignInInputsEmpty({ nick, password }) {
  if (!nick) {
    return 'nick is empty';
  }
  if (!password) {
    return 'password is empty';
  }

  return 'NOT_EMPTY';
}

module.exports = areSignInInputsEmpty;
