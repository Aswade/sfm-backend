const driver = require('../../dbDriver/dbDriver');
const dbDriver = new driver();
const bcrypt = require('bcrypt');
async function registerUser({ nick, password }) {
  let hashedPassword = await bcrypt.hash(password, 3);

  if (!hashedPassword) {
    return 'registration failed';
  }

  let isUserSaved = await dbDriver.insertUserToDb({
    nick: nick,
    password: hashedPassword,
  });

  if (isUserSaved !== 'user inserted to db') {
    return 'registration failed';
  }

  return 'REGISTERED';
}

module.exports = registerUser;
