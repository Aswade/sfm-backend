const areSignInInputsEmpty = require('./modules/areSignInInputsEmpty');
const logInUser = require('./modules/logInUser');

function signInHandler({ nick, password }) {
  const areInputsEmpty = areSignInInputsEmpty({ nick, password });

  if (areInputsEmpty !== 'NOT_EMPTY') {
    return { status: 400, response: areInputsEmpty };
  }
  const logInResult = logInUser({ nick, password });

  if (logInResult !== 'LOGED_IN') {
    return { status: 400, response: logInResult };
  }

  return { status: 200, response: 'log in successful' };
}

module.exports = signInHandler;
