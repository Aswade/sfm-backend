const areSubmitedInputsEmpty = require('./modules/areSubmitedInputsEmpty');
const insertSongToDbHandler = require('./modules/insertSongToDbHandler');
const moveFileToServer = require('./modules/moveFileToServer');

function uploadSongHandler({ author, title, genre, file }) {
  const areInputsEmpty = areSubmitedInputsEmpty({ author, title, genre, file });

  if (areInputsEmpty !== 'NOT_EMPTY') {
    return { status: 400, response: areInputsEmpty };
  }

  const moveFileResult = moveFileToServer(file);

  if (moveFileResult.result !== 'MOVED') {
    return { status: 500, response: moveFileResult.result };
  }
  const filePath = moveFileResult.filePath;
  const insertSongResult = insertSongToDbHandler({
    author,
    title,
    genre,
    filePath,
  });
  if (insertSongResult !== 'INSERTED') {
    return { status: 500, response: insertSongResult };
  }

  return { status: 200, response: 'song uploaded' };
}

module.exports = uploadSongHandler;
