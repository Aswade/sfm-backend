const express = require('express');
const router = express.Router();
const driver = require('../dbDriver/dbDriver');
const dbDriver = new driver();
const fileUpload = require('express-fileupload');
const fs = require('fs');
const signUpHandler = require('./signUpHandler');
const signInHandler = require('./signInHandler');
const uploadSongHandler = require('./uploadSongHandler');

router.use(fileUpload());

router.post('/signUp', async (req, res, next) => {
  let nick = req.body.nick || '';
  let password = req.body.password || '';
  let rePassword = req.body.rePassword || '';

  const signUpResult = await signUpHandler({ nick, password, rePassword });
  const status = signUpResult.status;
  const response = signUpResult.response;
  //console.log(signUpResult);
  res.status(status).json({ response });
});

router.post('/signIn', async (req, res) => {
  let nick = req.body.nick || '';
  let password = req.body.password || '';

  const signInResult = signInHandler({ nick, password });
  const status = signInResult.status;
  const response = signInResult.response;

  res.status(status).json({ response });
});

router.post('/uploadSong', async (req, res) => {
  if (!req.files) {
    res.status(500).json({ error: 'File was not submited' });
    return;
  }
  let submit = {};
  submit.author = req.body.author;
  submit.title = req.body.title;
  submit.genre = req.body.genre;
  submit.file = req.files.song || {};

  const uploadResult = uploadSongHandler(submit);
  const status = uploadResult.status;
  const response = uploadResult.response;

  res.status(status).json({ response });
});

module.exports = router;
