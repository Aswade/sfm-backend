const validateSignUpInputs = require('./modules/validateSignUpInputs');
const registerUser = require('./modules/registerUser');
async function signUpHandler(userInputs) {
  const { nick, password, rePassword } = userInputs;
  
  const validationResult = await validateSignUpInputs({ nick, password, rePassword });
  
  if (validationResult !== 'VALID') {
    return { status: 400, response: validationResult };
  }

  const registrationResult = registerUser({ nick, password });

  if (registrationResult !== 'REGISTERED') {
    return { status: 500, response: registrationResult };
  }

  return { status: 200, response: 'registration successful' };
}

module.exports = signUpHandler;
