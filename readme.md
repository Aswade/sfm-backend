# SFM - backend
## API + streaming service for SFM.
SFM - backend is an node.js API and streaming service that have standalone capabilities.


## Goals
- SFM - backend aims to provide a service that can stream music in base64 format and also independently recive requests, send responses from frontend. 

## Features

- Backend recives requests and send responses in JSON format. 
- Service utilizes MongoDB to easily store and read data about available music.
- Music is stored within service in specified folder so it can be transferred faster.

## Tech
SFM - backend uses a few of open source projects to work properly:

- [body-parser]
- [express-fileupload]
- [node.js]
- [Express.js]
- [mongoose]
